/**
 * This is a simple dynamic-layout UI library that's designed to make
 * layouts which resize as text changes size due to localisation easier
 * to make.
 * <p>
 * It takes heavy inspiration from Java's Swing.
 * <p>
 * See {@link xyz.znix.xftl.devutil.uiedit.UIEditor} for a development tool.
 */
package xyz.znix.xftl.ui;
